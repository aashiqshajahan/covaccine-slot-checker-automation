import time
from selenium import webdriver

PATH = "C:\chromedriver.exe"
driver = webdriver.Chrome(PATH)


def automation():
    driver.get('https://www.cowin.gov.in/home')
    time.sleep(2)

    pinBox = driver.find_element_by_xpath('//*[@id="mat-input-0"]')
    pinBox.send_keys('695026')

    searchButton = driver.find_element_by_xpath(
        '/html/body/app-root/div/app-home/div[2]/div/appointment-table/div/div/div/div/div/div/div/div/div/div/div[2]/form/div/div/div[2]/div[1]/div/button')
    searchButton.click()

    time.sleep(10)
    automation()


automation()
